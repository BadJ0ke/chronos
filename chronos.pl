#!/usr/bin/perl

use strict;
use Switch;
use IPC::Open3;
use Getopt::Long;
use POSIX qw(strftime);
use POSIX "sys_wait_h";
use Term::ANSIColor qw(:constants);
use Time::Local;
use File::Basename;


## Options

my $opt_help;
my $opt_day;
my $opt_date;
my $opt_week;
my $opt_print_rooms;
my $opt_no_print_rooms;

## Configuration

my $print_rooms = 1;

##################################################
## Time
##################################################

sub timestamp {
    my $date = shift;
    my @t = $date =~ m!(\d{2})/(\d{2})/(\d{4})!;
    $t[1]--;
    return timelocal 0,@t[undef,3,0,1,2];
}

sub get_date {
    return POSIX::strftime("%d/%m/%Y", localtime shift)
}

sub get_week_num {
    my $time = shift;
    my $week = POSIX::strftime("%W", localtime $time);
    $week += strftime("%m", localtime $time) >= 9 ? -36 : 16;
    return $week;
}

sub get_week_num_from_date {
    my $date = shift;
    my @t = $date =~ m!(\d{2})/(\d{2})/(\d{4})!;
    $t[1]--;
    my $timestamp = timelocal 0,@t[undef,3,0,1,2];
    my $week = POSIX::strftime("%W", localtime $timestamp);
    $week += strftime("%m", localtime $timestamp) >= 9 ? -36 : 16;
    return $week;
}

sub get_day_num {
    return POSIX::strftime("%u", localtime shift) - 1;
}

sub get_day_num_from_date {
    my $date = shift;
    my @t = $date =~ m!(\d{2})/(\d{2})/(\d{4})!;
    $t[1]--;
    my $timestamp = timelocal 0,@t[undef,3,0,1,2];
    my $day_num = POSIX::strftime("%u", localtime $timestamp);
    return ($day_num - 1);
}

sub compute_request {
    my $today = time;

    if (defined($opt_day))
    {
        $today += 24 * 3600 * $opt_day;
    }
    if (defined($opt_week))
    {
        $today += 7 * 24 * 3600 * $opt_week;
    }

    return $today;
}

sub get_str_day {
    my $day = shift;

    my $day_num = get_day_num_from_date($day);

    $day =~ /(.*)\/(.*)\/(.*)/;
    my $day = int($1);
    my $month = int($2);

    my $m;
    switch ($month) {
        case 1          { $m = "janvier"   }
        case 2          { $m = "février"   }
        case 3          { $m = "mars"      }
        case 4          { $m = "avril"     }
        case 5          { $m = "mai"       }
        case 6          { $m = "juin"      }
        case 7          { $m = "juillet"   }
        case 8          { $m = "août"      }
        case 9          { $m = "septembre" }
        case 10         { $m = "octobre"   }
        case 11         { $m = "novembre"  }
        case 12         { $m = "décembre"  }
        else            { $m = "unknown"   }
    }


    my $d;
    switch ($day_num) {
        case 0          { $d = "lundi"    }
        case 1          { $d = "mardi"    }
        case 2          { $d = "mercredi" }
        case 3          { $d = "jeudi"    }
        case 4          { $d = "vendredi" }
        case 5          { $d = "samedi"   }
        case 6          { $d = "dimanche" }
        else            { $d = "unknown"  }
    }

    return "$d $day $m $3";
}

# retrieve the first day of the week
# param: date
sub get_first_day {
    my $date = shift;
    my @t = $date =~ m!(\d{2})/(\d{2})/(\d{4})!;
    $t[1]--;
    my $timestamp = timelocal 0,@t[undef,3,0,1,2];

    while (get_day_num ($timestamp) > 0) {
        $timestamp -= 24 * 3600;
    }
    return $timestamp;
}

sub end_time {
    my $start = shift;
    my $duration = shift;

    $start =~ /(.*)h(.*)/;
    my $s_h = $1;
    my $s_m = $2;

    $duration =~ /(.*)h(.*)/;
    my $d_h = $1;
    my $d_m = $2;

    my $e = $s_h + $s_m / 60;
    $e += $d_h + $d_m / 60;
    my $e_h = int($e);
    my $e_m = ($e - $e_h) * 60;

    if (length($e_h) == 1) {
        $e_h = "0$e_h";
    }
    if (length($e_m) == 1) {
        $e_m = "0$e_m";
    }

    return "${e_h}h$e_m";
}

##################################################
## Folders
##################################################

sub init_folders () {
    -d '.chronos'
        || mkdir ('.chronos', 0755)
        || die ('Cannot write in folders\n');
    chdir ('.chronos');

    init_cache ();
}

##################################################
## Cache
##################################################

sub init_cache {
    -d 'cache'
        || mkdir ('cache', 0755);
}

sub get_cache {
    my $week = shift;

    -f "cache/$week" || return 1;

    my @stats = stat("cache/$week");

    (time - @stats[8] <= (60 * 20)) || return 2;
    return 0;
}

##################################################
## Chronos requests
##################################################

sub chronos_query {
    my $url = shift;
    my $data = shift;
    my $file = shift;

    my $cmd = "curl -s -b cookies.txt -c cookies.txt";

    if (defined($data)) {
        $cmd .= " --data \"$data\"";
    }
    $cmd .= " \"$url\"";

    if (defined($file)) {
        $cmd .= " | iconv -f ISO-8859-15 -t UTF8";
    }

    my $child_in;      # standard entry
    my $child_out;     # standard out
    my $child_err = 1; # standard error

    my $pid = IPC::Open3::open3($child_in, $child_out, $child_err, $cmd);

    my $ret = 0;
    my $start_time = time;
    my $timeout = 1;
    while ((time - $start_time < 10)) {

        if ($? != -1) {
            $ret = ($? >> 8);
        }

        if (waitpid($pid, WNOHANG) < 0) {
            print FILE_RET $ret;
            $timeout = 0;
            last;
        }
    }
    if ($timeout) {
        kill 1, $pid;
    } else {
        if (defined($file)) {
            open(FILE, ">$file");
            print FILE <$child_out>;
            close(FILE);
        }
    }

    return $timeout || $ret;
}

sub get_chronos_data {
    my $week = shift;

    chdir ('cache');

    my @queries = (
        ["http://chronos.epita.net"],
        ["http://chronos.epita.net/ade/custom/modules/plannings/direct_planning.jsp"],
        ["http://chronos.epita.net/ade/standard/gui/tree.jsp", "search=gra2"],
        ["http://chronos.epita.net/ade/custom/modules/plannings/bounds.jsp?week=$week&reset=true"],
        ["http://chronos.epita.net/ade/custom/modules/plannings/info.jsp?light=true", undef, "data.htm"]
        );

    for (my $i = 0; $i <= $#queries; $i++) {
        if (chronos_query ($queries[$i][0], $queries[$i][1], $queries[$i][2])) {
            print STDERR "Cannot reach Chronos\n";
            chdir ('..');
            return 0;
        }
        print ".";
    }
    print "\n";

    gen_planning ($week);
    chdir ('..');
    return 1;
}

sub gen_planning {
    my $week = shift;

    open (DATA, 'data.htm');
    open (WEEK, ">$week");

    while (<DATA>) {
        chomp;

        if (/<tr>.*<\/tr>/ && ! /.*Date.*/) {

            $_ =~ s/<\/?tr>//g;
            $_ = "</td>$_";        # manage first tag on the line
            $_ =~ s/<\/?a.*?>//g;  # remove link tags
            $_ =~ s/&nbsp;/ /g;

            my @fields = split("<\/td><td>", $_);

            for (my $i = 1; $i <= 7; $i++) {
                print WEEK @fields[$i];
                if ($i < 7) {
                    print WEEK "@@";
                }
            }
            print WEEK "\n";
        }
    }

    close (DATA);
    close (WEEK);

}

##################################################
## Display
##################################################

## print day indication ('today' or 'tomorrow') and date
## param: date
sub print_title {
    my $date = shift;
    my $prefix;

    if ($date == get_date(time)) {
        print GREEN;
        $prefix = "aujourd'hui ";
    } elsif ($date == get_date(time + 24 * 3600)) {
        print YELLOW;
        $prefix = "demain "
    }

    print "\n** $prefix".get_str_day ($date)." **\n";
}

sub print_day {
    my $date = shift;
    my $fd = shift;
    my $data = 0; ## number of classes

    print_title($date);
    print CLEAR;

    while (<$fd>) {
        my $l = length;
        chomp;
        my @fields = split("@@", $_);

        if (@fields[0] eq $date) {
            $data++;

            printf("%-6s- %-6s  %-60s\n",
                   @fields[1],
                   end_time (@fields[1], @fields[2]),
                   @fields[3]);

            if ($print_rooms) {
                printf ("  (%s)\n", @fields[6]);
            }

        } elsif (timestamp(@fields[0]) < timestamp($date)) {
            next
        } else {
            if (!$data) {
                print "aucun donnee\n";
            }

            seek ($fd, -$l, 1);
            return;
        }
    }

    if (!$data) {
        print "[aucun donnee]\n";
    }
}

sub print_planning {
    my $type = shift;
    my $date = shift;

    my $week = get_week_num_from_date($date);
    my $cache_status = get_cache($week);
    if ($cache_status) {
        if (!get_chronos_data ($week)) {
            if ($cache_status == 2) {
                print "Cached version is available\n";
            } else {
                return;
            }
        }
    }

    open (WEEK, "cache/$week") || die ("cache/$week");


    if ($type eq "DAY") {
        print_day ($date, \*WEEK);
    } elsif ($type eq "WEEK") {
        my $d = get_date(get_first_day($date));
        while (get_day_num_from_date($d) < 6) {
            print_day($d, \*WEEK);
            $d = get_date(timestamp($d) + 24 * 3600);
        }
        print_day($d, \*WEEK);
    }

    close (WEEK);
}

sub print_help {
    my @l = ("usage: ".basename($0)." [OPTIONS]",
             "Affiche l'emploi du temps Chronos sur la sortie standard.", "",
             "Options :\n",
             "-h, -help +N\t\taffiche cette aide\n",
             "-d, --day +N\t\taffiche le jour N apres aujourd'hui",
             "-d, --day -N\t\taffiche le jour N avant aujourd'hui\n",
             "-w, --week\t\taffiche la semaine entiere",
             "-w, --week +N\t\taffiche la semaine N apres la semaine actuelle",
             "-w, --week -N\t\taffiche la semaine N avant la semaine actuelle\n",
             "-r, --rooms\t\tforce l'affichage des salles",
             "-nr, --no-rooms\t\tdesactive l'affichage des salles"
);
    foreach (@l) { print; print "\n"; }
}

##################################################
##################################################
## Program
##################################################
##################################################

## Options

GetOptions('d|day:0' => \$opt_day,
           'a|date=s' => \$opt_date,
           'w|week:0' => \$opt_week,
           'rooms|r' => \$opt_print_rooms,
           'no-rooms|nr' => \$opt_no_print_rooms,
           'h|help' => \$opt_help);

if (defined($opt_help)) {
    print_help;
    exit;
}

if (defined($opt_week)) {
    $print_rooms = 0;
}

if (defined($opt_print_rooms)) {
    $print_rooms = 1;
}
if (defined($opt_no_print_rooms)) {
    $print_rooms = 0;
}

## Initialisations
init_folders ();

my $time = compute_request ();
my $date = get_date ($time);

# my $req_week = get_week_num ($time);
# my $req_day = get_date ($time);
# my $req_day_num = get_day_num ($time);

my $type;

if (defined($opt_week)) {
    $type = "WEEK";
} else {
    $type = "DAY";
}

print_planning ($type, $date);
